from cryptography.fernet import Fernet
import argparse
from pathlib import Path


class KeyFileAlreadyExists(Exception):
    def __str__(self):
        return "Key file already exists! Use --overwrite parameter to generate new key."


class Mcrypt:

    key_filepath: str | Path
    _key: bytes

    def __init__(self, key_filepath: Path | str):
        """
        Initializes new Mcrypt object.
        If path to file with secret key is not given, it will by default use <current_path>/.key file.
        :param key_filepath: Path to a file with encryption / decryption key
        """
        self.key_filepath = Path(key_filepath)
        if self.key_filepath.exists():
            self.read_key(key_filepath)

    def read_key(self, key_filepath: str | Path):
        """
        Reads encryption / decryption key from a file
        :param key_filepath:
        """
        with open(key_filepath, 'r', encoding="utf-8") as s:
            self._key = s.read().encode()
            if not self._key:
                print("Provided key is empty!")
                exit(1)

    def generate_secret_key(self, overwrite: bool = False) -> str:
        """
        A function that generates unique symmetric secret key used in encryption / decryption methods
        and saves to a file
        :param overwrite: If given output key file already exists: overwrites it (if True) or does nothing (if False)
        """

        if self.key_filepath.exists() and not overwrite:
            raise KeyFileAlreadyExists
        generated_key = Fernet.generate_key().decode()
        with open(self.key_filepath, 'w', encoding="utf-8") as key:
            key.write(generated_key)
        return str(self.key_filepath.resolve())

    def encrypt(self, text: str) -> str:
        """
        A function that encrypts given text string
        :param text: Plain text to encrypt
        :return: Encrypted string
        """
        fernet = Fernet(self._key)
        return fernet.encrypt(text.encode()).decode()

    def decrypt(self, text: str) -> str:
        """
        A function that decrypts given encrypted string
        :param text: An encrypted text to decrypt
        :return: Decrypted string
        """
        fernet = Fernet(self._key)
        return fernet.decrypt(text.encode()).decode()

    def encrypt_file(self, input_filepath: str | Path) -> str:
        """
        Encrypts file contents and saves them into a new file with '_encrypted' suffix
        :param input_filepath:
        :return:
        """
        output_filepath = Path(input_filepath + "_encrypted")
        with open(input_filepath, 'r', encoding="utf-8") as f:
            _input = f.read()
        _encrypted = self.encrypt(_input)
        with open (output_filepath, 'w', encoding="utf-8") as f:
            f.write(_encrypted)
        return str(output_filepath.resolve())

    def decrypt_file(self, input_filepath: str | Path) -> str:
        """
        Decrypts file contents and saves them into a new file with '_decrypted' suffix
        :param input_filepath:
        :return:
        """
        output_filepath = Path(input_filepath + "_decrypted")
        with open(input_filepath, 'r', encoding="utf-8") as f:
            _input = f.read()
        _decrypted = self.decrypt(_input)
        with open (output_filepath, 'w', encoding="utf-8") as f:
            f.write(_decrypted)
        return str(output_filepath.resolve())


def get_parser():
    _parser = argparse.ArgumentParser(description='Parse input parameters')

    _parser.add_argument('-k', '--key_path',
                         help='Secret key file path')

    _subparsers = _parser.add_subparsers(help='commands', dest='command')

    _subparser_gen = _subparsers.add_parser('key', help='Generates secret key')
    _subparser_gen.add_argument('--generate',
                                help='Generates secret key',
                                action='store_true')
    _subparser_gen.add_argument('--overwrite',
                                help='Overrides key if exists',
                                action='store_true')

    _subparser_encrypt = _subparsers.add_parser('encrypt', help='Encrypts given text or a file')
    _subparser_encrypt_excl = _subparser_encrypt.add_mutually_exclusive_group()
    _subparser_encrypt_excl.add_argument('-i', '--input',
                                         help="Text to encrypt")
    _subparser_encrypt_excl.add_argument('-f', '--file',
                                         help="File to encrypt")

    _subparser_decrypt = _subparsers.add_parser('decrypt', help='Decrypts given encryption or a file')
    _subparser_decrypt_excl = _subparser_decrypt.add_mutually_exclusive_group()
    _subparser_decrypt_excl.add_argument('-i', '--input',
                                         help="Text to decrypt")
    _subparser_decrypt_excl.add_argument('-f', '--file',
                                         help="File to decrypt")

    return _parser


def main():
    parser = get_parser()
    args = parser.parse_args()
    secret_key = args.key_path if args.key_path else Path(Path.cwd(), '.key')
    mcrypt = Mcrypt(secret_key)

    if args.command == "key":
        if args.generate:
            print(mcrypt.generate_secret_key(overwrite=args.overwrite))
    elif args.command == "encrypt":
        if args.input:
            print(mcrypt.encrypt(args.input))
        elif args.file:
            print(mcrypt.encrypt_file(args.file))
    elif args.command == "decrypt":
        if args.input:
            print(mcrypt.decrypt(args.input))
        elif args.file:
            print(mcrypt.decrypt_file(args.file))
    else:
        print(f"{args.command} - No or unknown argument")
        parser.print_help()
        exit(1)


if __name__ == "__main__":
    main()
