from mcrypt import Mcrypt, KeyFileAlreadyExists
import pathlib3x as pathlib
import pytest


class Tests:

    test_dir = pathlib.Path('tests')
    test_file = pathlib.Path(test_dir, 'test_file.txt')
    test_key = pathlib.Path(test_dir, '.key')
    test_text = "Ala ma kota \na kot ma Alę"

    def clean(self):
        self.test_dir.rmtree(ignore_errors=True)

    def prepare_mcrypt(self):
        self.clean()
        self.test_dir.mkdir(exist_ok=True)
        return Mcrypt(self.test_key)

    def prepare_file(self):
        with open(self.test_file, 'w', encoding="utf-8") as f:
            f.write(self.test_text)
        return str(self.test_file.resolve())

    def test_key_generation_no_overwrite(self):
        mcrypt = self.prepare_mcrypt()
        mcrypt.generate_secret_key(overwrite=True)
        with pytest.raises(KeyFileAlreadyExists):
            mcrypt.generate_secret_key(overwrite=False)

    def test_key_generation_overwrite(self):
        mcrypt = self.prepare_mcrypt()
        mcrypt.generate_secret_key(overwrite=True)
        mcrypt.generate_secret_key(overwrite=True)

    def test_mcrypt_input(self):
        mcrypt = self.prepare_mcrypt()
        mcrypt.generate_secret_key(overwrite=True)
        mcrypt.read_key(self.test_key)
        encrypted = mcrypt.encrypt(self.test_text)
        decrypted = mcrypt.decrypt(encrypted)
        assert self.test_text == decrypted

    def test_mcrypt_file(self):
        mcrypt = self.prepare_mcrypt()
        file_name = self.prepare_file()
        mcrypt.generate_secret_key(overwrite=True)
        mcrypt.read_key(self.test_key)
        encrypted = mcrypt.encrypt_file(file_name)
        decrypted = mcrypt.decrypt_file(encrypted)
        input_contents = open(file_name, 'r', encoding="utf-8").read()
        decrypted_contents = open(decrypted, 'r', encoding="utf-8").read()
        assert input_contents == decrypted_contents
